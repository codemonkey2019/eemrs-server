package com.liu.eemrsserver.utils.crypto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;

/**
 * 服务器端密钥集
 */
@AllArgsConstructor
@NoArgsConstructor
public class SMServerKey {
    @Getter
    private BCECPublicKey publicKey;
    @Getter
    private BCECPrivateKey privateKey;
    @Getter
    private byte[] sm4Key;
    @Getter
    private byte[] opeKey;
}
