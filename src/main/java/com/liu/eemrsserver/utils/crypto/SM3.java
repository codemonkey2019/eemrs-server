package com.liu.eemrsserver.utils.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * @author L
 * @date 2019-09-22 10:32
 * @desc
 **/
public class SM3 {

    private static MessageDigest messageDigest;
    static {
        try {
            messageDigest= MessageDigest.getInstance("SM4");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String hash(String data){
        return Base64.getEncoder().encodeToString(messageDigest.digest(data.getBytes()));
    }
}
