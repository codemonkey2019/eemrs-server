package com.liu.eemrsserver.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author L
 * @date 2019-10-19 10:42
 * @desc 自定义元组
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewPair {
    private String department;
    private String idNumber;
}
