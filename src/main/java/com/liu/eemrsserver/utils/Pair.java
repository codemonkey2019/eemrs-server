package com.liu.eemrsserver.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自定义泛型元组
 * @param <T>
 * @param <E>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pair<T,E> {
    private T key;
    private E value;
}
