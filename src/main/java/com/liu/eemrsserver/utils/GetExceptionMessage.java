package com.liu.eemrsserver.utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class GetExceptionMessage {
    /**
     * 提取异常信息中的核心信息
     * @param e
     * @return
     */
    public static String getMessage(Exception e){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        e.printStackTrace(new PrintStream(bos));
        String message = bos.toString();
        return message;
    }
}
