package com.liu.eemrsserver.service;

import com.liu.eemrsserver.crypto.UserManagerCrypto;
import com.liu.eemrsserver.jsontrans.UserLogin;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @author L
 * @date 2019-09-21 12:46
 * @desc 用户管理
 **/
@Service
@Scope("prototype")
public class UserOpService {
    @Autowired
    private UserManagerCrypto userOpCrypto;
    /**
     * 根据传经来的密文UserOp，解密后插入数据
     *
     * @param userOP
     * @return
     */
    public boolean insertUser(@NonNull UserLogin userOP) {
        return userOpCrypto.insertUserWithType(userOP);
    }
    public boolean loginUser(UserLogin userOP) {
        return userOpCrypto.loginUserWithType(userOP);
    }
    public boolean logoutUser(UserLogin userOP) {
        return userOpCrypto.logoutUserWithType(userOP);
    }
}
