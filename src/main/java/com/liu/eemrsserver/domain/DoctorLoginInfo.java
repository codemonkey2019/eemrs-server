package com.liu.eemrsserver.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * @author L
 * @date 2019-09-20 18:45
 * @desc 医生登录操作的信息
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorLoginInfo {
    @JSONField(serialize=false)
    private Integer id;

    private String idNumber;
    private String userName;
    private String department;

    @JSONField(serialize=false)
    private String password;
    @JSONField(serialize=false)
    private String idHashCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorLoginInfo docLog = (DoctorLoginInfo) o;
        return idNumber.equals(docLog.idNumber) &&
                department.equals(docLog.department) &&
                password.equals(docLog.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idNumber, password, userName);
    }
}
