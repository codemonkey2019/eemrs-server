/**
 * 逻辑层和持久层中间的加密层，负责加密逻辑层传下来的数据，然后再调用持久层
 */
package com.liu.eemrsserver.crypto;