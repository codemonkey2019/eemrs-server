package com.liu.eemrsserver.crypto;

import com.liu.eemrsserver.utils.crypto.SMServerKey;
import com.liu.eemrsserver.domain.DoctorLoginInfo;
import com.liu.eemrsserver.domain.PatientLoginInfo;
import com.liu.eemrsserver.mapper.UserLogMapper;
import com.liu.eemrsserver.jsontrans.UserLogin;
import com.liu.eemrsserver.utils.crypto.JavaBeanEnc;
import com.liu.eemrsserver.utils.crypto.SM3;
import lombok.NonNull;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author L
 * @date 2019-09-22 9:29
 * @desc 用户管理加解密
 **/
@Component("userOpCrypto")
@Scope("prototype")
public class UserManagerCrypto {
    private static Logger logger = Logger.getLogger(UserManagerCrypto.class);

    @Autowired
    private SMServerKey smServerKey;

    @Autowired
    private UserLogMapper userOpMapper;

    /**
     * 根据传来的密文UserOp，解密后插入数据,并返回密文响应值
     *
     * @param deUserOp
     * @return
     */
    public boolean insertUserWithType(@NonNull UserLogin deUserOp) {
        try {

            deUserOp.setHashCode(//计算hash值
                    SM3.hash(deUserOp.getIdNumber())
            );

            UserLogin userOPDo = JavaBeanEnc.encUserOp(deUserOp, smServerKey.getSm4Key());//加密POJO

            boolean out = userOpMapper.insertUserWithType(userOPDo);//插入并返回

            logger.info("插入结果：" + out);
            return out;
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return false;
    }

    public boolean loginUserWithType(UserLogin deUserOp) {

        String hashCode = SM3.hash(deUserOp.getIdNumber());
        if (deUserOp.getType().equals("dt")) {
            DoctorLoginInfo user = new DoctorLoginInfo(null, deUserOp.getIdNumber(), null, deUserOp.getDepartment(), deUserOp.getPassword(), hashCode);
            List<DoctorLoginInfo> enUser = userOpMapper.getDocByHash(hashCode);
            if (enUser.size() == 0) {
                return false;
            }
            DoctorLoginInfo deUser = JavaBeanEnc.decDoc(enUser.get(0), smServerKey.getSm4Key());
            boolean bool = deUser.equals(user);
            return bool;
        } else {
            PatientLoginInfo user = new PatientLoginInfo(null, deUserOp.getIdNumber(), null, deUserOp.getPassword(), hashCode);
            List<PatientLoginInfo> enUser = userOpMapper.getPatByHash(hashCode);
            if (enUser.size() == 0) {
                return false;
            }
            PatientLoginInfo deUser = JavaBeanEnc.decPat(enUser.get(0), smServerKey.getSm4Key());
            boolean bool = deUser.equals(user);
            return bool;
        }

    }

    public boolean logoutUserWithType(UserLogin deUserOp) {
        String hashCode = SM3.hash(deUserOp.getIdNumber());

        if (deUserOp.getType().equals("dt")) {
            DoctorLoginInfo user = new DoctorLoginInfo(null, deUserOp.getIdNumber(), null, deUserOp.getDepartment(), deUserOp.getPassword(), hashCode);
            List<DoctorLoginInfo> enUser = userOpMapper.getDocByHash(hashCode);
            if (enUser.size() == 0) {
                return false;
            }
            DoctorLoginInfo deUser = JavaBeanEnc.decDoc(enUser.get(0), smServerKey.getSm4Key());
            if (user.equals(deUser)) {
                return userOpMapper.deleteUserByHash(hashCode, deUserOp.getType());
            } else return false;
        } else {
            PatientLoginInfo user = new PatientLoginInfo(null, deUserOp.getIdNumber(), null, deUserOp.getPassword(), hashCode);
            List<PatientLoginInfo> enUser = userOpMapper.getPatByHash(hashCode);
            if (enUser.size() == 0) {
                return false;
            }
            PatientLoginInfo deUser = JavaBeanEnc.decPat(enUser.get(0), smServerKey.getSm4Key());
            if (user.equals(deUser)) {
                return userOpMapper.deleteUserByHash(hashCode, deUserOp.getType());
            } else return false;
        }

    }
}
