/*
Navicat MySQL Data Transfer

Source Server         : MySQL-Local
Source Server Version : 80018
Source Host           : localhost:3306
Source Database       : eemrs

Target Server Type    : MYSQL
Target Server Version : 80018
File Encoding         : 65001

Date: 2021-04-10 11:08:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_doctor
-- ----------------------------
DROP TABLE IF EXISTS `tb_doctor`;
CREATE TABLE `tb_doctor` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_number` varchar(255) NOT NULL,
  `id_hash_code` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tb_doctor_id_hash_code_uindex` (`id_hash_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for tb_guahao
-- ----------------------------
DROP TABLE IF EXISTS `tb_guahao`;
CREATE TABLE `tb_guahao` (
  `department` varchar(255) NOT NULL,
  `id_number` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `id_hash_code` varchar(255) NOT NULL,
  `doctor_id_hash_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for tb_medical_infor
-- ----------------------------
DROP TABLE IF EXISTS `tb_medical_infor`;
CREATE TABLE `tb_medical_infor` (
  `department` varchar(255) NOT NULL,
  `medication` varchar(255) NOT NULL,
  `condition_description` varchar(255) NOT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `visit_time` bigint(20) NOT NULL,
  `patient_name` varchar(255) NOT NULL,
  `patient_id_number` varchar(255) NOT NULL,
  `patient_id_hash_code` varchar(255) NOT NULL,
  `age` bigint(20) DEFAULT NULL,
  `doctor_name` varchar(255) NOT NULL,
  `doctor_id_number` varchar(255) NOT NULL,
  `doctor_id_hash_code` varchar(255) NOT NULL,
  `d_pk` text NOT NULL,
  `signature` text NOT NULL,
  `gender` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for tb_patient
-- ----------------------------
DROP TABLE IF EXISTS `tb_patient`;
CREATE TABLE `tb_patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) NOT NULL,
  `id_hash_code` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `medicare_card` varchar(255) DEFAULT NULL,
  `counter` text,
  `nation` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tb_patient_id_hash_code_uindex` (`id_hash_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
