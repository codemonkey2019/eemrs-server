# 密文电子病历系统系统环境搭建

**下面的配置要依次配置成功，不能跳着配**

## java环境  JDK 1.8

   其他较高版本的JDK需要额外配置JavaFX

**下载**

| Windows x64 | 211.54 MB | [jdk-8u251-windows-x64.exe](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html#license-lightbox) |
| ----------- | --------- | ------------------------------------------------------------ |
|             |           |                                                              |

**配置JDK**

https://www.jianshu.com/p/169bc950316b



---

## 集成开发环境  Idea

https://www.jetbrains.com/idea/

---

## Maven   项目管理工具

**下载：**

| Binary zip archive | [apache-maven-3.6.3-bin.zip](https://mirror.bit.edu.cn/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip) |
| ------------------ | ------------------------------------------------------------ |
|                    |                                                              |

**配置**

https://www.jianshu.com/p/62a76daf5096

注意配置本地仓库的位置

注意配置maven阿里镜像

https://blog.csdn.net/weixin_34850743/article/details/99441245

注意idea配置maven

https://jingyan.baidu.com/article/a3a3f811cd5f0b8da2eb8abf.html

---

## MySQL数据库

https://www.cnblogs.com/zaid/p/MySQL.html



# 部署启动项目

## 1、本项目代码地址

服务器端：https://gitee.com/codemonkey2019/eemrs-server.git

医生端：https://gitee.com/codemonkey2019/eemrs-doctor.git

病人端：https://gitee.com/codemonkey2019/eemrs-patient.git

## 2、项目启动

1. 创建数据库表

   1. 创建一个数据库,并切换到这个数据库

      ```mysql
      create database eemrs;
      use eemrs;
      ```

   2. 导入数据表：`src\main\resources\sql\eemrs.sql`

2. 修改项目中的数据库连接配置

   找到并打开： 项目根目录/src/main/resources/application.yml

   修改数据库密码

   ```yaml
   spring:
     datasource:
       username: root
       password: 你的密码
       url: jdbc:mysql:///eemrs?serverTimezone=Asia/Shanghai
       driver-class-name: com.mysql.cj.jdbc.Driver    
       
       #驱动配置根据你使用的驱动自行配置（mysql5和mysql8的驱动路径不同）
       #mysql 5    : com.mysql.jdbc.Driver
       #mysql 8    : com.mysql.cj.jdbc.Driver
   ```

3. 找到主类，启动项目

